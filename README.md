# README #
Pi from Pie

* Created this code to estimate the value of pi by simulating throwing of darts. Might be informative
* Version : Finalized and added the test function


### How do I get set up? ###

* Two functions, forPi and whilePi are cornerstones. forPi simulates dart throwing for a count of n times (can be changed as desired at line 63. Higher number takes longer). whilePi function takes in an input that's the max error range from actual value of Pi. Can be changed at line 64
* Python 2.7
* Requires random module and math module of python 2.7

### Who do I talk to? ###

* **Karthik Mohan**
* *email : karthik.mohan390@gmail.com*