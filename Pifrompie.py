# -*- coding: utf-8 -*-
"""
Created on Wed Sep 14 17:10:23 2016

@author: demerzel
"""

# python 2
#
# Pi from Pie!
#
# Name: Karthik Mohan
#
#----------------------------------------------------------------------------#
import random
import math
#----------------------------------------------------------------------------#

def dartThrow():
    """ This function simulates the throwing of a dart """
    x = random.uniform(-1.0,1.0)
    y = random.uniform(-1.0,1.0)
    dfo = ((x**2) + (y**2))**0.5
    if dfo < 1.0 :
        return True
    else: return False

#----------------------------------------------------------------------------#

def forPi( n ):
    """ This function returns the estimated value of Pi
        Input: n - int, is the number of throws of the dart
        Output : Estimate of the value of Pi """
    darthits = 0
    for i in xrange(n):
        throw = dartThrow()
        if throw == True:
            darthits += 1
        pisti = float(4)*(darthits)/(int(i) + 1)
        print darthits," hits out of ",(int(i) + 1)," throws so that pi is ",pisti
        continue
    return float(4)*(darthits)/(int(n))

#----------------------------------------------------------------------------#

def whilePi( maxerror ):
    """ Input : maxerror is a floating point number. Used as an ceiling for a max value of difference between estimated value of Pi and the actual value
        Output : Returns the estimated value of Pi within the error range """
    pisti = 0.0
    dartcount = 0
    darthits = 0
    while abs(math.pi - pisti) > maxerror :
        throw = dartThrow()
        dartcount += 1
        if throw == True:
            darthits += 1
        pisti = float(4)*(darthits)/(dartcount)
        print darthits," hits out of ",dartcount," throws so that pi is ",pisti
    return dartcount

#----------------------------------------------------------------------------#
def test():
    print forPi(100)
    print whilePi(0.1)

test()